import 'package:get_it/get_it.dart';
import 'package:soccer_app/model/interactor.dart';
import 'package:soccer_app/network/data/soccer_countries_network_data_source.dart';
import 'package:soccer_app/network/data/soccer_players_network_data_source.dart';
import 'package:soccer_app/network/dio_soccer_service.dart';
import 'package:soccer_app/network/soccer_api.dart';
import 'package:soccer_app/ui/country/countries_bloc.dart';
import 'package:soccer_app/ui/player/players_bloc.dart';

final injector = GetIt.instance;

void initDependencies() {
  injector.registerSingleton<SoccerApi>(SoccerService());

  injector.registerSingleton(CountryNetworkDataSource(injector<SoccerApi>()),);
  injector.registerSingleton(PlayerNetworkDataSource(injector<SoccerApi>()),);

  injector.registerSingletonAsync(
      () async {
        return Interactor(
          injector<CountryNetworkDataSource>(),
          injector<PlayerNetworkDataSource>(),
        );
      }
  );

  injector.registerFactory(
      () => CountriesBloc(
        injector<Interactor>(),
      ),
  );

  injector.registerFactory(
        () => PlayersBloc(
      injector<Interactor>(),
    ),
  );

}