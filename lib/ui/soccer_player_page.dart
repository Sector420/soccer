

import 'package:flutter/material.dart';
import 'package:soccer_app/model/player.dart';

class PlayerPage extends StatelessWidget {
  final Player player;

  PlayerPage({required this.player});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.lightBlue[100],
      appBar: AppBar(
        title: Text("${player.firstName} ${player.lastName}"),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(
          25.0
        ),
        child: ListView(
          children: [
             const Card(
              child: Image(
                image: AssetImage('assets/player.jpg'),
                fit: BoxFit.fitWidth,
              ),
            ),
            Card(
              child: ListTile(
                title: Text(
                  player.country.name,
                  style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 24.0),),
                trailing: Text(
                  player.country.continent ?? "",
                  style: const TextStyle(fontSize: 24.0),
                ),
              ),
            ),
            Card(
              child: ListTile(
                title: Text(
                  "birthday: ${player.birthDay}",
                  style: const TextStyle(fontSize: 16.0),),
                trailing: Text("${player.age} years old",
                  style: const TextStyle(fontSize: 16.0),),
                ),
              ),
            Card(
              child: ListTile(
                title: Text(
                  "Weight: ${player.weight},    Height: ${player.height}",
                  style: const TextStyle(fontSize: 16.0),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ],
        ),
      )
    );
  }
}