

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:soccer_app/di/di_utils.dart';
import 'package:soccer_app/ui/player/players_bloc.dart';
import 'package:soccer_app/ui/soccer_player_page.dart';

class PlayerList extends StatelessWidget {
  final int countryId;

  PlayerList({required this.countryId});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => injector<PlayersBloc>(),
      child: BlocBuilder<PlayersBloc, PlayersState>(
        builder: (context, state) {
          if (state is Loading) {
            BlocProvider.of<PlayersBloc>(context).add(LoadPlayersEvent(countryId));
            return const Scaffold(
              body: Center(
                child: CircularProgressIndicator(),
              ),
            );
          }
          if (state is ContentReady) {
            return Scaffold(
              appBar: AppBar(
                title: const Text("Players with chosen nationality"),
                centerTitle: true,
              ),
              body: ListView.builder(
                itemCount: state.players.length,
                itemBuilder: (context, index) {
                  return Card(
                    shadowColor: Colors.black54,
                    elevation: 16.0,
                    child: ListTile(
                      title: Text("${state.players[index].firstName} ${state.players[index].lastName}"),
                      leading: Text((index+1).toString(), style: const TextStyle(fontSize:24.0),),
                      onTap: () {
                        Navigator.push(
                          context, MaterialPageRoute(
                          builder: (context) => PlayerPage(player: state.players[index]),
                          ),
                        );
                      },
                    ),
                  );
                },
              ),
            );
          }
          return const Center(
            child: Text("Epic error"),
          );
        },
      ),
    );
  }
}