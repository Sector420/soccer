import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:soccer_app/di/di_utils.dart';
import 'package:soccer_app/ui/country/countries_bloc.dart';
import 'package:soccer_app/ui/soccer_playerlist_page.dart';
import 'package:soccer_app/ui/utils.dart';

class SoccerHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) => injector<CountriesBloc>(),
        child: BlocListener<CountriesBloc, CountriesState>(
          listener: (context, state) {
            if (state is Error) {
              context.showSnackBar(
                content: const Text("Failed HTTP request"),
              );
            }
          },
          child: BlocBuilder<CountriesBloc, CountriesState>(
            builder: (context, state) {
              if (state is Loading) {
                BlocProvider.of<CountriesBloc>(context).add(LoadCountriesEvent());
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
              if (state is Content) {
                return CountriesContent(state);
              }
              return const Center(
                child: Text("This fail is epic..."),
              );
            },
          ),
        ),
      ),
    );
  }
}


class CountriesContent extends StatefulWidget {
  final Content state;

  CountriesContent(this.state);

  @override
  _CountriesContentState createState() => _CountriesContentState(state);
}

class _CountriesContentState extends State<CountriesContent> with SingleTickerProviderStateMixin {
  Content state;
  var _scrollController;
  _CountriesContentState(this.state);

  @override
  void initState() {
    _scrollController = ScrollController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var list = state.countries;
    list.removeWhere((element) => element.countryCode == null);
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Soccer App - Countries",
          style: TextStyle(
            fontWeight: FontWeight.bold
          ),
        ),
        centerTitle: true,
      ),
      body: NestedScrollView(
        controller: _scrollController,
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              expandedHeight: 240.0,
              pinned: false,
              floating: true,
              flexibleSpace: const FlexibleSpaceBar(
                background: Image(
                  image: AssetImage('assets/soccer.jpg'),
                  fit: BoxFit.fitWidth,
                ),
              ),
              forceElevated: innerBoxIsScrolled,

            ),
          ];
        },
        body: ListView.builder(
          itemCount: list.length,
          itemBuilder: (context, index) {
            return Card(
              shadowColor: Colors.black45,
              elevation: 16.0,
              child: ListTile(
                title: Text(
                  list[index].name,
                  style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 36.0),
                ),
                subtitle: Text(
                  list[index].continent ?? "",
                  style: const TextStyle(fontSize: 18.0),
                ),
                trailing: Text(
                  list[index].countryCode ?? "",
                  style: const TextStyle(fontSize: 36.0, color: Colors.black54 ,fontWeight: FontWeight.bold, fontStyle: FontStyle.italic),
                ),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => PlayerList(countryId: list[index].id,),
                      ),
                  );
                },
              ),
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          context.showSnackBar(content: const Text("Made by Földvári Ádám Dávid\nPowered by SportDataApi"));
        },
        child: const Icon(Icons.sports_soccer),
        backgroundColor: Colors.lightBlue,
      ),
    );
  }
}
