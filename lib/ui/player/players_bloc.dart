import 'dart:async';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:soccer_app/model/player.dart';

part 'players_event.dart';
part 'players_state.dart';

class PlayersBloc extends Bloc<PlayersEvent, PlayersState> {
  final _interactor;

  PlayersBloc(this._interactor): super(Loading());

  @override
  Stream<PlayersState> mapEventToState(PlayersEvent event,) async* {
    if(event is LoadPlayersEvent) {
      yield* _mapLoadPlayersToState(event.countryId);
    }
  }

  Stream<PlayersState> _mapLoadPlayersToState(int countryId) async* {
    try {
      print("Getting Players by bloc");
      yield ContentReady(players: await _interactor.getPlayers(countryId));
    } on Exception catch (e) {
      print("HTTP error: ${e.toString()}");
      yield Error(players: []);
    }
  }
}