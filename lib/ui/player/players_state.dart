part of 'players_bloc.dart';


@immutable
abstract class PlayersState {
  const PlayersState();
}

class Loading extends PlayersState {
  static final Loading _instance = Loading._();
  factory Loading() => _instance;
  Loading._();
}

class Content extends PlayersState {
  final List<Player> players;
  Content({required this.players,});
}

class ContentReady extends Content with EquatableMixin {
  final List<Player> players;
  ContentReady({required this.players}): super(players: players);
  @override
  List<Object> get props => [players];
}

class Error extends Content with EquatableMixin {
  Error({required List<Player> players}): super(players: players);
  @override
  List<Object> get props => [players];
}
