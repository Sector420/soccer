part of 'players_bloc.dart';

@immutable
abstract class PlayersEvent {
  const PlayersEvent();
}

class LoadPlayersEvent extends PlayersEvent {
  final int countryId;
  LoadPlayersEvent(this.countryId);
}