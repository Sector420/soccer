part of 'countries_bloc.dart';

@immutable
abstract class CountriesEvent {
  const CountriesEvent();
}

class LoadCountriesEvent extends CountriesEvent {
  LoadCountriesEvent();
}