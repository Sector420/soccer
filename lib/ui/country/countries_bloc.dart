import 'dart:async';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:soccer_app/model/country.dart';

part 'countries_event.dart';
part 'countries_state.dart';

class CountriesBloc extends Bloc<CountriesEvent, CountriesState> {
  final _interactor;

  CountriesBloc(this._interactor): super(Loading());

  @override
  Stream<CountriesState> mapEventToState(CountriesEvent event,) async* {
    if(event is LoadCountriesEvent) {
      yield* _mapLoadCountriesToState();
    }
  }

  Stream<CountriesState> _mapLoadCountriesToState() async* {
    try {
      print("Getting Countries by bloc");
      yield ContentReady(countries: await _interactor.getCountries());
    } on Exception catch (e) {
      print("HTTP error: ${e.toString()}");
      yield Error(countries: []);
    }
  }
}