part of 'countries_bloc.dart';


@immutable
abstract class CountriesState {
  const CountriesState();
}

class Loading extends CountriesState {
  static final Loading _instance = Loading._();
  factory Loading() => _instance;
  Loading._();
}

class Content extends CountriesState {
  final List<Country> countries;
  Content({required this.countries,});
}

class ContentReady extends Content with EquatableMixin {
  final List<Country> countries;
  ContentReady({required this.countries}): super(countries: countries);
  @override
  List<Object> get props => [countries];
}

class Error extends Content with EquatableMixin {
  Error({required List<Country> countries}): super(countries: countries);
  @override
  List<Object> get props => [countries];
}
