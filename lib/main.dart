import 'package:flutter/material.dart';
import 'package:soccer_app/ui/soccer_home_page.dart';

import 'di/di_utils.dart';

void main() {
  initDependencies();
  WidgetsFlutterBinding.ensureInitialized();
  runApp(SoccerApp());
}

class SoccerApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: injector.allReady(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return MaterialApp(
            title: 'Soccer App',
            theme: ThemeData(
              primaryColor: Colors.lightBlue,
              visualDensity: VisualDensity.adaptivePlatformDensity,
            ),
            home: SoccerHomePage(),
          );
        }

        return Container(
          color: Colors.lightBlue,
          child: const Center(
            child: CircularProgressIndicator(),
          ),
        );
      }
    );
  }
}