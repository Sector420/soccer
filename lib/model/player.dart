import 'country.dart';

class Player {
  final int id;
  final String? firstName;
  final String? lastName;
  final String? birthDay;
  final int? age;
  final int? weight;
  final int? height;
  final Country country;

  Player({
    required this.id,
    this.firstName,
    this.lastName,
    this.birthDay,
    this.age,
    this.weight,
    this.height,
    required this.country,
  });

}

