class Country {
  final int id;
  final String name;
  final String? countryCode;
  final String? continent;

  Country({
    required this.id,
    required this.name,
    this.countryCode,
    this.continent,
  });

}