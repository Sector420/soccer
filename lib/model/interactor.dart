import 'package:flutter/foundation.dart';
import 'package:soccer_app/model/country.dart';
import 'package:soccer_app/model/player.dart';
import 'package:soccer_app/network/data/soccer_countries_network_data_source.dart';
import 'package:soccer_app/network/data/soccer_players_network_data_source.dart';

class Interactor {
  final CountryNetworkDataSource _countryNetworkDataSource;
  final PlayerNetworkDataSource _playerNetworkDataSource;

  Interactor(
      this._countryNetworkDataSource,
      this._playerNetworkDataSource,
      );

  Future<List<Country>> getCountries() async {
    debugPrint("Downloading Countries from network");
    return await _countryNetworkDataSource.getCountries() ?? [];
  }

  Future<List<Player>> getPlayers(int countryId) async {
    debugPrint("Downloading Players with the chosen nationality from network");
    return await _playerNetworkDataSource.getPlayers(countryId) ?? [];
  }
}