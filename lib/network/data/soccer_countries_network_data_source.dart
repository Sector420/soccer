import 'package:soccer_app/model/country.dart';
import 'package:soccer_app/network/data/json_model/countries_json_models.dart';

import '../soccer_api.dart';

class CountryNetworkDataSource {
  final SoccerApi _soccerApi;

  CountryNetworkDataSource(this._soccerApi);

  Future<List<Country>?> getCountries() async {
    final networkResponse = await _soccerApi.getCountries();

    if(networkResponse.response.statusCode != 200) return null;

    final networkCountries = networkResponse.data.data;

    return networkCountries.map((country) => country.toDomainModel()).toList();
  }
}

extension on SoccerApiCountry {
  Country toDomainModel() {
    return Country(
      id: id,
      name: name,
      countryCode: countryCode,
      continent: continent,
    );
  }
}