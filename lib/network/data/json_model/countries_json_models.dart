import 'package:json_annotation/json_annotation.dart';

part 'countries_json_models.g.dart';

@JsonSerializable()
class SoccerApiCountriesResponse {
  final List<SoccerApiCountry> data;

  SoccerApiCountriesResponse({
    required this.data,
  });
  factory SoccerApiCountriesResponse.fromJson(Map<String, dynamic> json) => _$SoccerApiCountriesResponseFromJson(json);
  Map<String, dynamic> toJson() => _$SoccerApiCountriesResponseToJson(this);
}

@JsonSerializable()
class SoccerApiCountry {
  @JsonKey(name: "country_id")
  final int id;
  final String name;
  @JsonKey(name: "country_code", includeIfNull: true)
  final String? countryCode;
  @JsonKey(name: "continent", includeIfNull: true)
  final String? continent;

  SoccerApiCountry({
    required this.id,
    required this.name,
    this.countryCode,
    this.continent,
  });
  factory SoccerApiCountry.fromJson(Map<String, dynamic> json) => _$SoccerApiCountryFromJson(json);
  Map<String, dynamic> toJson() => _$SoccerApiCountryToJson(this);
}