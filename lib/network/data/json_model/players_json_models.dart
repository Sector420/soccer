import 'package:json_annotation/json_annotation.dart';

import 'countries_json_models.dart';


part 'players_json_models.g.dart';

@JsonSerializable()
class SoccerApiPlayersResponse {
  final List<SoccerApiPlayer> data;

  SoccerApiPlayersResponse({
    required this.data,
  });
  factory SoccerApiPlayersResponse.fromJson(Map<String, dynamic> json) => _$SoccerApiPlayersResponseFromJson(json);
  Map<String, dynamic> toJson() => _$SoccerApiPlayersResponseToJson(this);
}

@JsonSerializable()
class SoccerApiPlayer {
  @JsonKey(name: "player_id")
  final int id;
  final String? firstname;
  final String? lastname;
  final String? birthday;
  final int? age;
  final int? weight;
  final int? height;
  final String? img;
  final SoccerApiCountry country;

  SoccerApiPlayer({
    required this.id,
    this.firstname,
    this.lastname,
    this.birthday,
    this.age,
    this.weight,
    this.height,
    this.img,
    required this.country,
  });
  factory SoccerApiPlayer.fromJson(Map<String, dynamic> json) => _$SoccerApiPlayerFromJson(json);
  Map<String, dynamic> toJson() => _$SoccerApiPlayerToJson(this);
}