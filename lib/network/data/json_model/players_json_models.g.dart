// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'players_json_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SoccerApiPlayersResponse _$SoccerApiPlayersResponseFromJson(
    Map<String, dynamic> json) {
  return SoccerApiPlayersResponse(
    data: (json['data'] as List<dynamic>)
        .map((e) => SoccerApiPlayer.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$SoccerApiPlayersResponseToJson(
        SoccerApiPlayersResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
    };

SoccerApiPlayer _$SoccerApiPlayerFromJson(Map<String, dynamic> json) {
  return SoccerApiPlayer(
    id: json['player_id'] as int,
    firstname: json['firstname'] as String?,
    lastname: json['lastname'] as String?,
    birthday: json['birthday'] as String?,
    age: json['age'] as int?,
    weight: json['weight'] as int?,
    height: json['height'] as int?,
    img: json['img'] as String?,
    country: SoccerApiCountry.fromJson(json['country'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$SoccerApiPlayerToJson(SoccerApiPlayer instance) =>
    <String, dynamic>{
      'player_id': instance.id,
      'firstname': instance.firstname,
      'lastname': instance.lastname,
      'birthday': instance.birthday,
      'age': instance.age,
      'weight': instance.weight,
      'height': instance.height,
      'img': instance.img,
      'country': instance.country,
    };
