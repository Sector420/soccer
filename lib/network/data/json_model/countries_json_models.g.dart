// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'countries_json_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SoccerApiCountriesResponse _$SoccerApiCountriesResponseFromJson(
    Map<String, dynamic> json) {
  return SoccerApiCountriesResponse(
    data: (json['data'] as List<dynamic>)
        .map((e) => SoccerApiCountry.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$SoccerApiCountriesResponseToJson(
        SoccerApiCountriesResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
    };

SoccerApiCountry _$SoccerApiCountryFromJson(Map<String, dynamic> json) {
  return SoccerApiCountry(
    id: json['country_id'] as int,
    name: json['name'] as String,
    countryCode: json['country_code'] as String?,
    continent: json['continent'] as String?,
  );
}

Map<String, dynamic> _$SoccerApiCountryToJson(SoccerApiCountry instance) =>
    <String, dynamic>{
      'country_id': instance.id,
      'name': instance.name,
      'country_code': instance.countryCode,
      'continent': instance.continent,
    };
