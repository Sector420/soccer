

import 'package:soccer_app/model/country.dart';
import 'package:soccer_app/model/player.dart';
import 'json_model/countries_json_models.dart';
import 'soccer_countries_network_data_source.dart';
import '../soccer_api.dart';
import 'json_model/players_json_models.dart';

class PlayerNetworkDataSource {
  final SoccerApi _soccerApi;

  PlayerNetworkDataSource(this._soccerApi);

  Future<List<Player>?> getPlayers(int countryId) async {
    final networkResponse = await _soccerApi.getPlayers(countryId);

    if(networkResponse.response.statusCode != 200) return null;

    final networkPlayers = networkResponse.data.data;

    return networkPlayers.map((player) => player.toDomainModel()).toList();
  }
}

extension on SoccerApiPlayer {
  Player toDomainModel() {
    return Player(
      id: id,
      firstName: firstname,
      lastName: lastname,
      birthDay: birthday,
      age: age,
      weight: weight,
      height: height,
      country: country.toDomainModel(),

    );
  }
}

extension on SoccerApiCountry {
  Country toDomainModel() {
    return Country(
      id: id,
      name: name,
      countryCode: countryCode,
      continent: continent,
    );
  }
}