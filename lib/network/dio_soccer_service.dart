import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';
import 'package:retrofit/retrofit.dart';
import 'package:soccer_app/network/data/json_model/countries_json_models.dart';
import 'package:soccer_app/network/data/json_model/players_json_models.dart';
import 'package:soccer_app/network/soccer_api.dart';

part 'dio_soccer_service.g.dart';

@RestApi(baseUrl: "https://app.sportdataapi.com/api/v1")
abstract class SoccerService implements SoccerApi {
  factory SoccerService() {
    final _dio = Dio();

    _dio.interceptors.add(
        InterceptorsWrapper(
            onRequest: (options, handler) {
              options.queryParameters["apikey"] =
              "a1872d50-5d9c-11ec-98b0-ff8f710b0209";
              handler.next(options);
            }
        )
    );
    _dio.interceptors.add(LogInterceptor());
    return _SoccerService(_dio);
  }

  @override
  @GET("/soccer/countries")
  Future<HttpResponse<SoccerApiCountriesResponse>> getCountries();

  @override
  @GET("/soccer/players")
  Future<HttpResponse<SoccerApiPlayersResponse>> getPlayers(@Query("country_id")int countryId);
}