import 'dart:async';

import 'package:retrofit/retrofit.dart';
import 'package:soccer_app/network/data/json_model/countries_json_models.dart';
import 'package:soccer_app/network/data/json_model/players_json_models.dart';

abstract class SoccerApi {
  Future<HttpResponse<SoccerApiCountriesResponse>> getCountries();
  Future<HttpResponse<SoccerApiPlayersResponse>> getPlayers(int countryId);
}